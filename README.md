### What is this repository for? ###

Basic database with indices for education.

### How do I get set up? ###

* Compile the projects
* Run DBServer and the Gateway webserver
* To add some rows, run the TransactionTest
* View the table with a web browser, the url e.g. http://localhost:9000/Results?count=100&start=0