﻿using Core;
using DBLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DBServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = new NetServer();
            var schema = server.CreateTableFromSchema("highscores.xml");
            if (schema != null)
            {
                SortedIndex<int> byScore = new SortedIndex<int>(server.Database, schema.FindByName("Score"));
                NonUniqueHashIndex byUserID = new NonUniqueHashIndex(server.Database, schema.FindByName("UserID"),
                    () => new SortedIndex<int>(schema.FindByName("Score")));
                NonUniqueHashIndex byDifficulty = new NonUniqueHashIndex(server.Database, schema.FindByName("Difficulty"),
                    () => new SortedIndex<int>(schema.FindByName("Score")));
                NonUniqueHashIndex byLevel = new NonUniqueHashIndex(server.Database, schema.FindByName("Level"),
                    () => new SortedIndex<int>(schema.FindByName("Score")));

                server.Database.AddIndex(byScore, new IndexSpec()
                    {
                        Sort = new SortBy()
                        {
                            Field = schema.FindByName("Score"),
                            Order = SortOrder.Decending,
                        },
                    });

                server.Database.AddIndex(byUserID, new IndexSpec()
                    {
                        Sort = new SortBy()
                        {
                            Field = schema.FindByName("Score"),
                            Order = SortOrder.Decending,
                        },
                        Filter = schema.FindByName("UserID"),
                    });

                server.Database.AddIndex(byDifficulty, new IndexSpec()
                {
                    Sort = new SortBy()
                    {
                        Field = schema.FindByName("Score"),
                        Order = SortOrder.Decending,
                    },
                    Filter = schema.FindByName("Difficulty"),
                });
                
                server.Database.AddIndex(byLevel, new IndexSpec()
                {
                    Sort = new SortBy()
                    {
                        Field = schema.FindByName("Score"),
                        Order = SortOrder.Decending,
                    },
                    Filter = schema.FindByName("Level"),
                });

                server.Start();
            }

            var health = new Health();
            health.Start("localhost", 63321, 8999, "DBServer");
        }
    }
}
