﻿using DBLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Timers;

namespace DBLib
{
    public class AutoRetryConnection
    {
        private NetConnection connection;
        public event EventHandler ConnectionUp;
        public event EventHandler ConnectionDown;

        private string host;
        private int port;
        private bool connected;
        private System.Timers.Timer timer;

        public AutoRetryConnection(string host, int port)
        {
            this.host = host;
            this.port = port;

            this.timer = new System.Timers.Timer(5000);
            this.timer.Enabled = false;
            this.timer.Elapsed += timer_Elapsed;
            this.timer.AutoReset = true;

            this.TryConnect();
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.timer.Enabled = false;
            this.TryConnect();
        }

        private bool TryConnect()
        {
            System.Diagnostics.Trace.WriteLine(string.Format("Trying to connect to {0}:{1}", host, port), "INFO");
            this.connection = new NetConnection(host, port);
            this.connection.Error += connection_Error;
            if (this.connection.ConnectionActive)
            {
                System.Diagnostics.Trace.WriteLine(string.Format("Connected to {0}:{1}", host, port), "INFO");
                this.connected = true;
                this.connection.ConnectionDropped += connection_ConnectionDropped;
                if (this.ConnectionUp != null)
                {
                    this.ConnectionUp(this, EventArgs.Empty);
                }
            }
            else
            {
                this.timer.Enabled = true;
            }
            return this.connection.ConnectionActive;
        }

        void connection_Error(object sender, DBLib.ErrorEventArgs e)
        {
            System.Diagnostics.Trace.WriteLine(string.Format("{0} - {1}:{2}", e.Message, host, port), "ERROR");
        }

        void connection_ConnectionDropped(object sender, EventArgs e)
        {
            System.Diagnostics.Trace.WriteLine(string.Format("Diconnected from {0}:{1}", host, port), "INFO");
            if (this.connected)
            {
                this.connected = false;
                if (this.ConnectionDown != null)
                {
                    this.ConnectionDown(this, EventArgs.Empty);
                }
                this.connection.ConnectionDropped -= connection_ConnectionDropped;
            }
            this.TryConnect();
        }

        public NetConnection Connection
        {
            get
            {
                return this.connection;
            }
        }

        public bool Connected
        {
            get
            {
                return this.connected;
            }
        }
    }
}
