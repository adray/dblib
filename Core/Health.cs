﻿using DBLib;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;

namespace Core
{
    public enum MonitorMessages : byte
    {
        Register,
        Query,
        QueryAll,
        Watch,
    }

    public class RemoteProcess
    {
        public string Name { get; private set; }
        public int Port { get; private set; }
        public string Status { get; private set; }
        public string Host { get; private set; }

        public RemoteProcess(string name, string status, int port, string host)
        {
            this.Name = name;
            this.Status = status;
            this.Port = port;
            this.Host = host;
        }
    }
    
    public class RemoteProcessEventArgs : EventArgs
    {
        public RemoteProcess Process { get; private set; }

        public RemoteProcessEventArgs(RemoteProcess process)
        {
            this.Process = process;
        }
    }

    public class Health
    {
        public event EventHandler<RemoteProcessEventArgs> ProcessUp;

        private AutoRetryConnection health;
        private int inboundPort;
        private string name;
        private List<string> watchList = new List<string>();

        public void Start(string host, int port, int inboundPort, string name)
        {
            this.name = name;
            this.inboundPort = inboundPort;
            health = new AutoRetryConnection(host, port);
            if (health.Connected)
            {
                OnConnected();
            }
            health.ConnectionDown += health_ConnectionDown;
            health.ConnectionUp += health_ConnectionUp;
        }

        void health_ConnectionDown(object sender, EventArgs e)
        {
            health.Connection.MessageRecieved -= Connection_MessageRecieved;
        }

        void Connection_MessageRecieved(object sender, MessageRecievedEventArgs e)
        {
            if (e.Message != null && (MonitorMessages)e.Message.Payload[0] == MonitorMessages.Watch)
            {
                e.Queue = false;

                if (this.ProcessUp != null)
                {
                    using (MemoryStream memory = new MemoryStream(e.Message.Payload))
                    {
                        using (BinaryReader reader = new BinaryReader(memory))
                        {
                            byte type = reader.ReadByte();
                            byte result = reader.ReadByte();
                            if (result == 1)
                            {
                                string name = reader.ReadString();
                                int port = reader.ReadInt32();
                                string status = reader.ReadString();
                                string host = reader.ReadString();
                                if (status == "UP")
                                {
                                    this.ProcessUp(this, new RemoteProcessEventArgs(new RemoteProcess(name, status, port, host)));
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                e.Queue = true;
            }
        }

        void health_ConnectionUp(object sender, EventArgs e)
        {
            OnConnected();
        }

        private void OnConnected()
        {
            System.Diagnostics.Trace.WriteLine(string.Format("Connected to health service."), "INFO");
            health.Connection.MessageRecieved += Connection_MessageRecieved;

            NetMessage msg = new NetMessage();
            byte[] buffer = new byte[100];
            using (MemoryStream memory = new MemoryStream(buffer))
            {
                using (BinaryWriter writer = new BinaryWriter(memory))
                {
                    writer.Write((byte)MonitorMessages.Register);
                    writer.Write(name);
                    writer.Write(inboundPort);
                }
            }
            msg.Payload = buffer;
            health.Connection.SendAsync(msg);

            foreach (var process in this.watchList)
            {
                SubscribeToWatch(process);
            }
        }

        public void WatchProcess(string process)
        {
            System.Diagnostics.Trace.WriteLine(string.Format("Watching {0}.", process), "INFO");
            watchList.Add(process);
            SubscribeToWatch(process);
        }

        private void SubscribeToWatch(string process)
        {
            if (health.Connected)
            {
                NetMessage msg = new NetMessage();
                byte[] buffer = new byte[100];
                using (MemoryStream memory = new MemoryStream(buffer))
                {
                    using (BinaryWriter writer = new BinaryWriter(memory))
                    {
                        writer.Write((byte)MonitorMessages.Watch);
                        writer.Write(process);
                    }
                }
                msg.Payload = buffer;
                health.Connection.SendAsync(msg);
            }
        }

        public List<RemoteProcess> GetAllProcesses()
        {
            var list = new List<RemoteProcess>();

            if (health.Connected)
            {
                NetMessage msg = new NetMessage();
                byte[] buffer = new byte[100];
                using (MemoryStream memory = new MemoryStream(buffer))
                {
                    using (BinaryWriter writer = new BinaryWriter(memory))
                    {
                        writer.Write((byte)MonitorMessages.QueryAll);
                    }
                }
                msg.Payload = buffer;

                NetMessage reply = this.health.Connection.SendRecieve(msg);

                if (reply != null)
                {
                    using (MemoryStream memory = new MemoryStream(reply.Payload))
                    {
                        using (BinaryReader reader = new BinaryReader(memory))
                        {
                            byte type = reader.ReadByte();
                            byte count = reader.ReadByte();
                            for (int i = 0; i < count; i++)
                            {
                                string name = reader.ReadString();
                                int port = reader.ReadInt32();
                                string status = reader.ReadString();
                                string host = reader.ReadString();
                                list.Add(new RemoteProcess(name, status, port, host));
                            }
                        }
                    }
                }
            }

            return list;
        }

        public RemoteProcess GetProcess(string name)
        {
            RemoteProcess process = null;

            if (health.Connected)
            {
                NetMessage msg = new NetMessage();
                byte[] buffer = new byte[100];
                using (MemoryStream memory = new MemoryStream(buffer))
                {
                    using (BinaryWriter writer = new BinaryWriter(memory))
                    {
                        writer.Write((byte)MonitorMessages.Query);
                        writer.Write(name);
                    }
                }
                msg.Payload = buffer;

                NetMessage reply = this.health.Connection.SendRecieve(msg);

                if (reply != null)
                {
                    using (MemoryStream memory = new MemoryStream(reply.Payload))
                    {
                        using (BinaryReader reader = new BinaryReader(memory))
                        {
                            byte type = reader.ReadByte();
                            byte result = reader.ReadByte();
                            if (result == 1)
                            {
                                reader.ReadString(); //name
                                int port = reader.ReadInt32();
                                string status = reader.ReadString();
                                string host = reader.ReadString();
                                process = new RemoteProcess(name, status, port, host);
                            }
                        }
                    }
                }
            }

            return process;
        }

        public void Shutdown()
        {
            if (this.health.Connected)
            {
                this.health.Connection.Close();
            }
        }
    }
}
