﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Core
{
    public static class Logger
    {
        private class TraceSource : System.Diagnostics.TraceListener
        {
            private StreamWriter writer;

            public TraceSource(string name)
            {
                this.writer = new StreamWriter(string.Format("{0}_Log.txt", name));
                this.TraceOutputOptions = System.Diagnostics.TraceOptions.DateTime | System.Diagnostics.TraceOptions.ThreadId;
            }

            public override void Write(string message)
            {
                writer.Write(message);
            }

            public override void WriteLine(string message)
            {
                writer.Write(message + Environment.NewLine);
            }

            public override void Flush()
            {
                writer.Flush();
            }
        }

        public static void Initialize(string name)
        {
            System.Diagnostics.Trace.Listeners.Add(new TraceSource(name));
            System.Diagnostics.Trace.AutoFlush = true;
        }
    }
}
