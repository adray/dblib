﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace DBLib
{
    internal enum MessageType
    {
        Heartbeart,
        Normal,
    }

    public class NetMessage
    {
        private byte[] header;
        private byte[] payload;

        public int Size
        {
            get
            {
                return Payload == null ? 0 : Payload.Length;
            }
        }

        public byte[] Header
        {
            get
            {
                return header;
            }
        }

        public NetMessage()
        {
            header = new byte[sizeof(long) + sizeof(byte)];
            this.MessageType = DBLib.MessageType.Normal;
            GenHeader();
        }

        internal MessageType MessageType
        {
            get
            {
                return (MessageType)header[sizeof(long)];
            }
            set
            {
                header[sizeof(long)] = (byte)value;
            }
        }

        private void GenHeader()
        {
            Array.Copy(BitConverter.GetBytes((long)Size + 9), header, sizeof(long));
        }

        public byte[] Payload
        {
            get
            {
                return this.payload;
            }
            set
            {
                this.payload = value;
                GenHeader();
            }
        }
    }

    public class MessageRecievedEventArgs : EventArgs
    {
        public NetMessage Message { private set; get; }
        public bool Queue { get; set; }

        public MessageRecievedEventArgs(NetMessage msg)
        {
            Message = msg;
            Queue = false;
        }
    }

    public class ErrorEventArgs : EventArgs
    {
        public string Message { get; private set; }

        public ErrorEventArgs(string msg)
        {
            Message = msg;
        }
    }

    public class NetConnection
    {
        private string host;
        private int port;
        private Socket socket;

        private int end = 0;
        private long length = -1;
        private Byte[] buffer = new byte[10000];

        public event EventHandler<ErrorEventArgs> Error;
        public event EventHandler<MessageRecievedEventArgs> MessageRecieved;
        public event EventHandler ConnectionDropped;

        private int state = 0;
        private int connectionDropped = 0;
        private BlockingCollection<NetMessage> queue = new BlockingCollection<NetMessage>();

        #region HealthCheck
        private System.Timers.Timer timer;

        private void InitializeHealthCheck()
        {
            this.RecieveAsync();

            this.timer = new System.Timers.Timer(10000);
            this.timer.Elapsed += timer_Elapsed;
            this.timer.Enabled = true;
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.timer.Enabled = false;

            // Send heartbeat.
            NetMessage reply = new NetMessage();
            reply.MessageType = MessageType.Heartbeart;

            this.SendAsync(reply);

            if (this.ConnectionActive)
            {
                this.timer.Enabled = true;
            }
        }
        #endregion

        public NetConnection(string host, int port)
        {
            this.host = host;
            this.port = port;

            var addresses = System.Net.Dns.GetHostAddresses(host);
            foreach (var address in addresses)
            {
                var endpoint = new System.Net.IPEndPoint(address, port);
                Setup(endpoint);
                if (socket != null)
                {
                    this.InitializeHealthCheck();
                    break;
                }
            }
        }

        internal NetConnection(Socket socket)
        {
            this.socket = socket;
            this.host = (this.socket.RemoteEndPoint as System.Net.IPEndPoint).Address.ToString();
            this.InitializeHealthCheck();
        }

        private void OnError(string msg)
        {
            if (this.Error != null)
            {
                this.Error(this, new ErrorEventArgs(msg));
            }
        }

        private void Setup(System.Net.IPEndPoint endpoint)
        {
            // Try to connect to the endpoint on any of the network interfaces.
            // In theory if the user has set up the routing tables correctly, this should not be needed...
            var interfaces = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
            foreach (var inter in interfaces)
            {
                try
                {
                    var addresses = inter.GetIPProperties().UnicastAddresses;
                    foreach (var address in addresses)
                    {
                        if (address.Address.AddressFamily == endpoint.AddressFamily)
                        {
                            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                            socket.ReceiveTimeout = 5000;
                            socket.Bind(new System.Net.IPEndPoint(address.Address, 0));
                            socket.Connect(endpoint);
                            return;
                        }
                    }
                }
                catch (System.Net.Sockets.SocketException ex)
                {
                    OnError(string.Format("Failed to connect {0} : {1}", endpoint.ToString(), ex.Message.ToString()));
                    if (socket != null)
                        socket.Dispose();
                    socket = null;
                }
            }
        }

        public bool ConnectionActive
        {
            get
            {
                if (socket != null)
                {
                    return socket.Connected;
                }
                return false;
            }
        }

        public NetMessage SendRecieve(NetMessage msg)
        {
            if (SendAsyncInternal(msg))
            {
                return this.RecieveNext();
            }
            return null;
        }

        public void SendAsync(NetMessage msg)
        {
            this.SendAsyncInternal(msg);
        }

        private bool SendAsyncInternal(NetMessage msg)
        {
            // Very careful this operation can be called on different threads.
            try
            {
                // This block is safe as only accesses msg and socket sendasync.
                byte[] array = new byte[msg.Size + msg.Header.Length];
                Array.Copy(msg.Header, array, msg.Header.Length);
                if (msg.Payload != null)
                {
                    Array.Copy(msg.Payload, 0, array, msg.Header.Length, msg.Payload.Length);
                }

                var e = new SocketAsyncEventArgs();
                e.SetBuffer(array, 0, array.Length);
                e.Completed += e_Completed;
                socket.SendAsync(e);
                return true;
            }
            catch (Exception)
            {
                this.OnConnectionDropped();
                return false;
            }
        }

        void e_Completed(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError != SocketError.Success)
            {
                this.Close();
            }
        }

        private NetMessage RecieveNext()
        {
            return this.queue.Take();
        }

        /// <summary>
        /// Starts recieves messages async.
        /// </summary>
        private void RecieveAsync()
        {
            if (Interlocked.CompareExchange(ref state, 1, 0) == 0)
            {
                RecieveAsyncInternal();
            }
            else
            {
                OnError("Already recieving async.");
            }
        }

        private void RecieveAsyncInternal()
        {
            if (state == 1)
            {
                try
                {
                    var e = new SocketAsyncEventArgs();
                    e.SetBuffer(buffer, end, buffer.Length - end);
                    e.Completed += eve_Completed;
                    if (!socket.ReceiveAsync(e))
                    {
                        // Has completed sync
                        ProcessRecieve(e, false);
                    }
                }
                catch (Exception ex)
                {
                    OnError(ex.Message);
                }
            }
        }

        private void CancelRecieveAsync()
        {
            if (Interlocked.CompareExchange(ref state, 0, 1) == 0)
            {
                OnError("Recieve async was not running.");
            }
        }

        void eve_Completed(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                this.ProcessRecieve(e, true);
            }
            else
            {
                this.OnMessageRecieved(null);
                this.Close();
            }
        }

        private void ProcessRecieve(SocketAsyncEventArgs e, bool async)
        {
            end += e.BytesTransferred;

            this.ProccessData(async);
        }

        private void ProccessData(bool async)
        {
            while ((length == -1 && end >= 8) || (end >= length && length != -1))
            {
                if (length == -1 && end >= 8)
                {
                    length = BitConverter.ToInt64(buffer, 0);
                    System.Diagnostics.Debug.Assert(length >= 0 && length < buffer.Length - 9);
                }

                if (end >= length && length != -1)
                {
                    NetMessage msg = new NetMessage();
                    msg.Payload = new byte[length-9];
                    msg.MessageType = (MessageType)buffer[8];
                    if (length > msg.Header.Length)
                    {
                        Array.Copy(buffer, 9, msg.Payload, 0, (int)length - 9);
                    }

                    // Reset data.
                    Array.Copy(buffer, (int)length, buffer, 0, buffer.Length - ((int)length));
                    end = end - (int)length;
                    length = -1;

                    this.OnMessageRecieved(msg);
                }
            }

            if (async)
            {
                this.RecieveAsyncInternal();
            }
        }

        protected virtual void OnMessageRecieved(NetMessage msg)
        {
            // Must guarantee this will only invoked at one thread at a time.
            bool isHeartbeart = msg != null && msg.Size == 0 && msg.MessageType == MessageType.Heartbeart;

            if (!isHeartbeart)
            {
                bool queue = true;

                if (this.MessageRecieved != null)
                {
                    var e = new MessageRecievedEventArgs(msg);
                    this.MessageRecieved(this, e);
                    queue = e.Queue;
                }
                
                if (queue)
                {
                    this.queue.Add(msg);
                }
            }
        }

        protected virtual void OnConnectionDropped()
        {
            // Will only execute once exactly.
            if (Interlocked.Increment(ref connectionDropped) == 1)
            {
                if (this.ConnectionDropped != null)
                {
                    this.ConnectionDropped(this, EventArgs.Empty);
                }
            }
        }

        public void Close()
        {
            try
            {
                this.socket.Close();
            }
            catch (Exception ex)
            {
                OnError(ex.Message);
            }
            finally
            {
                this.OnConnectionDropped();
            }
        }

        public string Address
        {
            get
            {
                return this.host;
            }
        }
    }

    public class NetConnectionEventArgs : EventArgs
    {
        public NetConnection Connection { get; private set; }
 
        public NetConnectionEventArgs(NetConnection connection)
        {
            this.Connection = connection;
        }
    }

    public class NetListener
    {
        /// <summary>
        /// Listener socket.
        /// </summary>
        private Socket socket;

        public event EventHandler<NetConnectionEventArgs> Added;

        public NetListener(int port)
        {
            System.Diagnostics.Trace.WriteLine(string.Format("Accepting connection on port {0}.", port), "INFO");
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Bind(new System.Net.IPEndPoint(System.Net.IPAddress.Any, port));
        }

        public void Listen()
        {
            var greetingServer = new Thread(new ThreadStart(() =>
            {
                socket.Listen(5);
                while (true)
                {
                    Socket remote = socket.Accept();
                    this.OnAdded(new NetConnection(remote));
                }
            }));
            greetingServer.Name = "GreetingServer";
            greetingServer.Start();
        }

        private void OnAdded(NetConnection conn)
        {
            System.Diagnostics.Trace.WriteLine(string.Format("Accepted connection from {0}", conn.Address), "INFO");
            if (this.Added != null)
            {
                this.Added(this, new NetConnectionEventArgs(conn));
            }
        }
    }
}
