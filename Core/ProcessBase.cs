﻿using DBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    public abstract class ProcessBase
    {
        private Health healthCheck;
        private string name;
        private int port;

        public ProcessBase(string name, int port)
        {
            this.name = name;
            this.port = port;
        }

        public void Start()
        {
            Logger.Initialize(name);

            this.healthCheck = new Health();
            this.healthCheck.ProcessUp += healthCheck_ProcessUp;
            this.healthCheck.Start("localhost", 63321, port, name);

            this.OnStart();
        }

        protected abstract void OnStart();

        void healthCheck_ProcessUp(object sender, RemoteProcessEventArgs e)
        {
            OnWatchedProcessUp(e.Process);
        }

        protected virtual void OnWatchedProcessUp(RemoteProcess process)
        {
            // Do nothing.
        }

        public void ConnectTo(string name)
        {
            RemoteProcess process = this.healthCheck.GetProcess(name);
            if (process != null && process.Status == "UP")
            {
                OnWatchedProcessUp(process);
            }
            else
            {
                this.healthCheck.WatchProcess(name);
            }
        }        

        public List<RemoteProcess> GetAllProcesses()
        {
            return this.healthCheck.GetAllProcesses();
        }
    }
}
