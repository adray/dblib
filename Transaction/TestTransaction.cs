﻿using DBLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Transaction
{
    public class TestTransaction
    {
        private IDBConnection connection = DBFactory.CreateConnection("localhost", 8999);
        
        public void EnterData()
        {
            var schema = connection.DownloadSchema("mySchema");
            connection.Schema = schema;

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Random rnd = new Random();

            for (int i = 0; i < 100000; i++)
            {
                var row = schema.CreateEmptyRow();
                row.SetField("Name", "Adam");
                row.SetField("Score", rnd.Next(1000000));

                if (rnd.Next(2) == 0)
                {
                    row.SetField("Difficulty", "Easy");
                }
                else
                {
                    row.SetField("Difficulty", "Hard");
                }

                connection.AtomicAdd(row);
            }

            //IRowMatcher matcher = new RowMatcher()
            //    .LimitBy(0, 100)
            //    .SortBy(schema.FindByName("Score"), SortOrder.Decending)
            //    .FilterBy(schema.FindByName("Difficulty"), "Hard");
            //IList<DataRow> rows = connection.AtomicLookup(matcher);

            //long add = stopwatch.ElapsedMilliseconds;

            //for (int i = 0; i < 10000; i++)
            //{
            //    DataRow rowObtained = connection.AtomicGetById(i);
            //}

            //long lookup = stopwatch.ElapsedMilliseconds;

            //for (int i = 0; i < 10000; i++)
            //{
            //    connection.AtomicDeleteById(i);
            //}

            //long delete = stopwatch.ElapsedMilliseconds;
            
            //stopwatch.Stop();

            //Console.WriteLine("Add " + add + "ms");
            //Console.WriteLine("Lookup " + lookup + "ms");
            //Console.WriteLine("Delete " + delete + "ms");
            //Console.ReadLine();
        }
    }
}
