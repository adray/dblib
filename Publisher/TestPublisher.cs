﻿using DBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Publisher
{
    public class TestPublisher
    {
        private IDBConnection connection = DBFactory.CreateConnection("localhost", 8999);

        public void Publish()
        {
            var schema = connection.DownloadSchema("mySchema");
            connection.Schema = schema;

            connection.RowAdded += connection_RowAdded;
        }

        void connection_RowAdded(object sender, RowChangeEventArgs<DataRow> e)
        {
            Console.WriteLine(e.Row.GetField<String>("Name") + "/" + e.Row.GetField<int>("Score").ToString());
        }
    }
}
