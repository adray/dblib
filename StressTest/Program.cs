﻿using DBLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace StressTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Stress.StartTest();
        }
        
        public class Stress
        {
            private IDBConnection conn = DBFactory.CreateConnection("localhost", 8999);
            private static IRowSchema schema;

            public static void StartTest()
            {                
                var dummy = DBFactory.CreateConnection("localhost", 8999);
                schema = dummy.DownloadSchema("mySchema");
                dummy.CloseConnection();

                for (int j = 0; j < 10; j++)
                {
                    Stopwatch watch = new Stopwatch();
                    watch.Start();

                    Thread[] threads = new Thread[8];
                    int i = 0;
                    for (; i < threads.Length / 2; i++)
                    {
                        threads[i] = new Thread(() =>
                        {
                            Stress stress = new Stress();
                            try
                            {
                                stress.Run();
                                stress.AddRows();
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Thread down");
                            }
                            finally
                            {
                                stress.conn.CloseConnection();
                            }
                        });
                        threads[i].Start();
                    }

                    for (; i < threads.Length; i++)
                    {
                        threads[i] = new Thread(() =>
                        {
                            Stress stress = new Stress();
                            try
                            {
                                stress.Run();
                                stress.Read();
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Thread down");
                            }
                            finally
                            {
                                stress.conn.CloseConnection();
                            }
                        });
                        threads[i].Start();
                    }

                    for (i = 0; i < threads.Length / 2; i++)
                    {
                        threads[i].Join();
                    }
                    Console.WriteLine("Completed in {0}ms", watch.ElapsedMilliseconds);
                }
                Console.ReadLine();
            }

            void Run()
            {
                this.conn.Schema = schema;
            }

            void Read()
            {
                for (int i = 0; i < 1000; i++)
                {
                    IRowMatcher matcher = new RowMatcher()
                        .LimitBy(0, 100)
                        .SortBy(schema.FindByName("Score"), SortOrder.Decending);
                    this.conn.AtomicLookup(matcher);
                }
            }

            void AddRows()
            {
                for (int i = 0; i < 1000; i++)
                {
                    var row = schema.CreateEmptyRow();
                    row.SetField("Name", "Foo");
                    row.SetField("Score", i * 100);
                    this.conn.AtomicAdd(row);
                }
            }
        }
    }
}
