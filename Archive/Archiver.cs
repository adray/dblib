﻿using Core;
using DBLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Archive
{
    class Archiver : SubscriberProcess
    {
        private StreamWriter writer = new StreamWriter("data.txt");
        private AutoResetEvent ev = new AutoResetEvent(false);
        private IRowSchema schema;

        public Archiver() : base("Archiver", 9555, "DBServer")
        {

        }
        
        protected override void OnStart()
        {
            base.OnStart();
            ev.WaitOne();
        }

        protected override void OnDatabaseAvailable()
        {
 	        base.OnDatabaseAvailable();
            if (this.schema == null)
            {
                this.schema = this.DBConnection.DownloadSchema("mySchema");
            }

            if (this.schema != null && this.DBConnection.Schema == null)
            {
                this.DBConnection.Schema = this.schema;
                this.DBConnection.RowAdded += DBConnection_RowAdded;
            }
        }

        void DBConnection_RowAdded(object sender, RowChangeEventArgs<DataRow> e)
        {
            OnRowAdded(e.Row);
        }

        private void OnRowAdded(DataRow row)
        {
            string name = row.GetField<string>("Name");
            string score = row.GetField<int>("Score").ToString();
            string difficulty = row.GetField<string>("Difficulty");
            writer.WriteLine("{0},{1},{2}", name, score, difficulty);
            writer.Flush();
        }
    }
}
