﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DBLib;
using System.IO;
using Core;
using System.Timers;

namespace Monitor
{
    public class MonitorServer
    {
        private NetListener listener = new NetListener(63321);
        private IDatabase database;
        private IRowSchema schema;
        
        public void Start()
        {
            Logger.Initialize("Monitor");

            schema = DBFactory.CreateSchema("monitor");
            schema.AddString("Name");
            schema.AddString("Address");
            schema.AddInt("Port");
            schema.AddString("State");

            database = schema.CreateDatabase();
            
            var index = new UniqueHashIndex(database, schema.FindByName("Name"));
            var spec = new IndexSpec()
            {
                Filter = schema.FindByName("Name"),
            };

            database.AddIndex(index, spec);

            var indexSort = new SortedIndex<string>(database, schema.FindByName("Name"));
            var specSort = new IndexSpec()
            {
                Sort = new SortBy()
                {
                    Field = schema.FindByName("Name"),
                    Order = SortOrder.Decending,
                }
            };

            database.AddIndex(indexSort, specSort);

            // Accept connections                        
            listener.Added += listener_Added;
            listener.Listen();
        }

        void listener_Added(object sender, NetConnectionEventArgs e)
        {
            new RemoteProcess(e.Connection, schema, database);
        }

        private class RemoteProcess
        {
            private NetConnection connection;
            private string name;
            private int port;
            private string address;
            private IDatabase database;
            private IRowSchema schema;
            private HashSet<string> watchList = new HashSet<string>();

            public RemoteProcess(NetConnection connection, IRowSchema schema, IDatabase database)
            {
                this.connection = connection;
                this.schema = schema;
                this.database = database;
                // Accept requests
                connection.Error += connection_Error;
                connection.MessageRecieved += Connection_MessageRecieved;
                connection.ConnectionDropped += Connection_ConnectionDropped;
            }
            
            void connection_Error(object sender, DBLib.ErrorEventArgs e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message, "ERROR");
            }

            void Connection_ConnectionDropped(object sender, EventArgs e)
            {
                System.Diagnostics.Trace.WriteLine(string.Format("Connection dropped to {0}.", connection.Address), "INFO");

                UpdateEntry("DOWN");

                this.StopPublisher();
                this.connection = null;
            }

            void Connection_MessageRecieved(object sender, MessageRecievedEventArgs e)
            {
                if (e.Message == null)
                    return;

                NetConnection connection = sender as NetConnection;
                System.Diagnostics.Trace.WriteLine(string.Format("Message recieved from {0}.", connection.Address), "INFO");

                using (MemoryStream memory = new MemoryStream(e.Message.Payload))
                {
                    using (BinaryReader reader = new BinaryReader(memory))
                    {
                        switch ((MonitorMessages)reader.ReadByte())
                        {
                            case MonitorMessages.Register:
                                {
                                    name = reader.ReadString();
                                    port = reader.ReadInt32();
                                    address = connection.Address;

                                    UpdateEntry("UP");
                                }
                                break;
                            case MonitorMessages.Query:
                                {
                                    var name = reader.ReadString();
                                    var matcher = new RowMatcher().FilterBy(schema.FindByName("Name"), name).LimitBy(0, 1);
                                    var row = database.AtomicLookup(matcher).FirstOrDefault();

                                    SendRow(MonitorMessages.Query, row);
                                }
                                break;
                            case MonitorMessages.QueryAll:
                                {
                                    var matcher = new RowMatcher().LimitBy(0, 100).SortBy(schema.FindByName("Name"), SortOrder.Decending);
                                    var rows = this.database.AtomicLookup(matcher);

                                    NetMessage reply = new NetMessage();
                                    reply.Payload = new byte[100];
                                    using (MemoryStream @out = new MemoryStream(reply.Payload))
                                    {
                                        using (BinaryWriter writer = new BinaryWriter(@out))
                                        {
                                            writer.Write((byte)MonitorMessages.QueryAll);
                                            if (rows != null)
                                            {
                                                writer.Write((byte)rows.Count);
                                                foreach (var row in rows)
                                                {
                                                    writer.Write((string)row.GetField(schema.FindByName("Name")));
                                                    writer.Write((int)row.GetField(schema.FindByName("Port")));
                                                    writer.Write((string)row.GetField(schema.FindByName("State")));
                                                    writer.Write((string)row.GetField(schema.FindByName("Address")));
                                                }
                                            }
                                            else
                                            {
                                                writer.Write((byte)0);
                                            }
                                        }
                                    }

                                    connection.SendAsync(reply);
                                }
                                break;
                            case  MonitorMessages.Watch:
                                {
                                    var process = reader.ReadString();
                                    System.Diagnostics.Trace.WriteLine(string.Format("{0} is watching {1}.", name, process), "INFO");

                                    this.StartPublisher();
                                    watchList.Add(process);

                                    // If already in the database send the current state.
                                    var matcher = new RowMatcher().FilterBy(schema.FindByName("Name"), process).LimitBy(0, 1);
                                    var row = database.AtomicLookup(matcher).FirstOrDefault();
                                    if (row != null)
                                    {
                                        SendRow(MonitorMessages.Watch, row);
                                    }
                                }
                                break;
                        }
                    }
                }
            }

            private void SendRow(MonitorMessages msg, IRow row)
            {
                NetMessage reply = new NetMessage();
                reply.Payload = new byte[100];
                using (MemoryStream @out = new MemoryStream(reply.Payload))
                {
                    using (BinaryWriter writer = new BinaryWriter(@out))
                    {
                        writer.Write((byte)msg);
                        if (row != null)
                        {
                            writer.Write((byte)1);
                            writer.Write((string)row.GetField(schema.FindByName("Name")));
                            writer.Write((int)row.GetField(schema.FindByName("Port")));
                            writer.Write((string)row.GetField(schema.FindByName("State")));
                            writer.Write((string)row.GetField(schema.FindByName("Address")));
                        }
                        else
                        {
                            writer.Write((byte)0);
                        }
                    }
                }

                connection.SendAsync(reply);
            }

            private void StartPublisher()
            {
                if (watchList.Count == 0)
                {
                    this.database.RowAdded += database_RowChange;
                    this.database.RowUpdated += database_RowChange;
                }
            }

            private void StopPublisher()
            {
                if (watchList.Count != 0)
                {
                    watchList.Clear();
                    this.database.RowAdded -= database_RowChange;
                    this.database.RowUpdated -= database_RowChange;
                }
            }

            void database_RowChange(object sender, RowChangeEventArgs<IPersistentRow> e)
            {
                string name = e.Row.GetField(schema.FindByName("Name")) as string;
                if (this.watchList.Contains(name))
                {
                    SendRow(MonitorMessages.Watch, e.Row);
                }
            }

            private void UpdateEntry(string state)
            {
                if (name != null)
                {
                    var matcher = new RowMatcher().FilterBy(schema.FindByName("Name"), name).LimitBy(0, 1);
                    var row = database.AtomicLookup(matcher).FirstOrDefault() as IPersistentRow;

                    var updateRow = schema.CreateEmptyRow();
                    updateRow.SetField("Name", name);
                    updateRow.SetField("Address", address);
                    updateRow.SetField("Port", port);
                    updateRow.SetField("State", state);

                    System.Diagnostics.Trace.WriteLine(string.Format("Status update {0} {1}.", name, state), "INFO");

                    if (row == null)
                    {
                        this.database.AtomicAddRow(updateRow);
                    }
                    else
                    {
                        this.database.AtomicUpdateRow(row.RowID, updateRow);
                    }
                }
            }
        }
    }
}
