﻿using Core;
using DBLib;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DBLib
{
    public class NetServer
    {
        private class NetThread
        {
            private List<IDBEndPoint> connections = new List<IDBEndPoint>(10);
            private AsyncQueue queue = new AsyncQueue();
            private IDatabase database;
            private IRowSchema schema;
            private int count;

            public NetThread(IDatabase database, IRowSchema schema)
            {
                this.database = database;
                this.schema = schema;
            }

            public void AddConnection(NetConnection conn)
            {
                Interlocked.Increment(ref count);
                var endpoint = DBFactory.CreateEndPoint(conn);
                this.queue.Queue(() => this.connections.Add(endpoint));
                this.QueueAsync(endpoint);
            }

            private void QueueAsync(IDBEndPoint endpoint)
            {
                this.queue.Queue(() => endpoint.ProcessAsync(this.database, this.schema));
            }

            public int Count
            {
                get
                {
                    return this.count;
                }
            }
        }

        private NetThread pool;
        private NetListener listener = new NetListener(8999);
        private IRowSchema schema;
        private IDatabase database;

        public IDatabase Database
        {
            get
            {
                return database;
            }
        }

        public NetServer()
        {
            Logger.Initialize("Database");
        }

        public IRowSchema CreateTableFromSchema(string schemaXmlFile)
        {
            var doc = new System.Xml.XmlDocument();
            doc.Load(schemaXmlFile);

            var schemaNode = doc.SelectSingleNode("Schema");
            if (schemaNode != null)
            {
                schema = DBFactory.CreateSchema(schemaNode.Attributes["Name"].Value);
                foreach (System.Xml.XmlNode fieldNode in schemaNode.SelectNodes("Field"))
                {
                    var type = fieldNode.Attributes["Type"].Value.ToLower();
                    var name = fieldNode.Attributes["Name"].Value;
                    if (type == "string")
                    {
                        schema.AddString(name);
                    }
                    else if (type == "integer")
                    {
                        schema.AddInt(name);
                    }
                }

                database = schema.CreateDatabase();
                return schema;
            }
            return null;
        }

        public void Start()
        {
            this.Allocate();
            listener.Added += listener_Added;
            listener.Listen();
        }

        void listener_Added(object sender, NetConnectionEventArgs e)
        {
            pool.AddConnection(e.Connection);            
        }

        private void Allocate()
        {
            pool = new NetThread(this.database, this.schema);
        }
    }
}
