﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBLib
{
    public abstract class SubscriberProcess : ProcessBase
    {
        private string dbServer;
        public IDBConnection DBConnection { get; private set; }

        protected override void OnStart()
        {
            this.ConnectTo(this.dbServer);
        }

        public SubscriberProcess(string name, int port, string dbServer) : base(name, port)
        {
            this.dbServer = dbServer;
        }

        protected override void OnWatchedProcessUp(RemoteProcess process)
        {
            if (process.Name == dbServer)
            {
                if (DBConnection == null)
                {
                    DBConnection = DBFactory.CreateConnection(process.Host, process.Port);
                }
                OnDatabaseAvailable();
            }
        }

        protected virtual void OnDatabaseAvailable()
        {
            // Do nothing
        }
    }
}
