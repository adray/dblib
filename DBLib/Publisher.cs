﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBLib
{
    public class Publisher
    {
        private NetConnection connection;
        private IDatabase database;
        private IRowSchema schema;
        private AsyncQueue queue = new AsyncQueue();

        public Publisher(IDatabase database, IRowSchema schema, NetConnection connection)
        {
            this.database = database;
            this.connection = connection;
            this.schema = schema;
            this.database.RowAdded += database_RowAdded;
            this.database.RowDeleted += database_RowDeleted;
            this.database.RowUpdated += database_RowUpdated;
        }

        void database_RowUpdated(object sender, RowChangeEventArgs<IPersistentRow> e)
        {
            queue.Queue(() =>
            {
                SendRow(e);
            });
        }

        void database_RowDeleted(object sender, RowChangeEventArgs<IPersistentRow> e)
        {
            queue.Queue(() =>
            {
                SendRow(e);
            });
        }

        void database_RowAdded(object sender, RowChangeEventArgs<IPersistentRow> e)
        {
            queue.Queue(() =>
                {
                    SendRow(e);
                });
        }

        void SendRow(RowChangeEventArgs<IPersistentRow> e)
        {
            IRow row = e.Row;

            NetMessage msg = new NetMessage();
            RowWriter writer = new RowWriter();
            writer.WriteInt(e.Update);
            schema.WriteRow(writer, row);
            msg.Payload = writer.Data;

            connection.SendAsync(msg);
        }
    }
}
