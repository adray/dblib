﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DBLib
{
    public interface IRowSchema
    {
        void AddInt(string name);
        void AddString(string name);
        int FindByName(string name);
        IDatabase CreateDatabase();
        byte[] ToBytes();
        DataRow CreateEmptyRow();
        DataRow CreateRow(RowReader reader);
        void WriteRow(RowWriter writer, IRow row);
        byte[] WriteRowMatcher(IRowMatcher row);
        IRowMatcher ReadRowMatcher(BinaryReader reader);
    }

    internal class RowSchema : IRowSchema
    {
        private enum FieldType
        {
            Int,
            String,
        }

        private class FieldDef
        {
            public string Name { get; set; }
            public FieldType Type { get; set; }
        }

        private string name;
        private List<FieldDef> fields = new List<FieldDef>();

        public RowSchema(string name)
        {
            this.name = name;
        }

        public RowSchema(string name, byte[] bytes)
        {
            this.name = name;

            using (MemoryStream memory = new MemoryStream(bytes))
            {
                using (BinaryReader reader = new BinaryReader(memory))
                {
                    int count = reader.ReadByte();
                    for (int i = 0; i < count; i++)
                    {
                        FieldDef def = new FieldDef();
                        int length = reader.ReadByte();
                        def.Name = Encoding.Default.GetString(reader.ReadBytes(length));
                        def.Type = (FieldType)reader.ReadByte();
                        fields.Add(def);
                    }
                }
            }
        }

        public IDatabase CreateDatabase()
        {
            Database database = new Database();

            foreach (var field in fields)
            {
                if (field.Type == FieldType.Int)
                {
                    database.AddInt(field.Name);
                }
                else if (field.Type == FieldType.String)
                {
                    database.AddString(field.Name);
                }
            }

            return database;
        }

        public void AddInt(string name)
        {
            fields.Add(new FieldDef()
            {
                Name = name,
                Type = FieldType.Int,
            });
        }

        public void AddString(string name)
        {
            fields.Add(new FieldDef()
            {
                Name = name,
                Type = FieldType.String,
            });
        }

        public int FindByName(string name)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                if (fields[i].Name == name)
                    return i;
            }
            return -1;
        }

        public byte[] ToBytes()
        {
            int length = 1;

            foreach (var field in this.fields)
            {
                length += Encoding.Default.GetByteCount(field.Name) + 1;
                length += 1; // for type
            }

            byte[] buffer = new byte[length];

            using (MemoryStream memory = new MemoryStream(buffer))
            {
                using (BinaryWriter writer = new BinaryWriter(memory))
                {
                    writer.Write((byte)this.fields.Count);
                    foreach (var field in this.fields)
                    {
                        byte[] bytes = Encoding.Default.GetBytes(field.Name);
                        writer.Write((byte)bytes.Length);
                        writer.Write(bytes);
                        writer.Write((byte)field.Type);
                    }
                }
            }

            return buffer;
        }

        public void WriteRow(RowWriter writer, IRow row)
        {
            writer.WriteFieldCount(this.fields.Count);
            for (int i = 0; i < this.fields.Count; i++)
            {
                var field = this.fields[i];
                if (field.Type == FieldType.Int)
                {
                    writer.WriteInt(row.GetField(i));
                }
                else if (field.Type == FieldType.String)
                {
                    writer.WriteString(row.GetField(i));
                }
            }
        }

        public DataRow CreateRow(RowReader reader)
        {
            var row = new DataRow();

            for (int i = 0; i < this.fields.Count; i++)
            {
                var field = this.fields[i];
                if (field.Type == FieldType.Int)
                {
                    row.AddInt(field.Name);
                    row.SetField(i, reader.ReadInt());
                }
                else if (field.Type == FieldType.String)
                {
                    row.AddString(field.Name);
                    row.SetField(i, reader.ReadString());
                }
            }

            return row;
        }

        public DataRow CreateEmptyRow()
        {
            var row = new DataRow();

            foreach (var field in fields)
            {
                if (field.Type == FieldType.Int)
                {
                    row.AddInt(field.Name);
                }
                else if (field.Type == FieldType.String)
                {
                    row.AddString(field.Name);
                }
            }

            return row;
        }

        public byte[] WriteRowMatcher(IRowMatcher row)
        {
            var mapper = new TypeMapperDefault();

            int length = sizeof(int) * 3;

            if (row.Limit != null)
            {
                length += 2 * sizeof(int);
            }

            if (row.Filter != null)
            {
                length += sizeof(int);
                var field = this.fields[row.Filter.Field];
                if (field.Type == FieldType.Int)
                {
                    length += sizeof(int);
                }
                else if (field.Type == FieldType.String)
                {
                    length += mapper.MeasureString(row.Filter.Value);
                }
            }

            if (row.Sort != null)
            {
                length += 2 * sizeof(int);
            }

            byte[] array = new byte[length];

            using (MemoryStream mem = new MemoryStream(array))
            {
                using (BinaryWriter wr = new BinaryWriter(mem))
                {
                    mapper.WriteInt(wr, row.Limit != null ? 1 : 0);
                    if (row.Limit != null)
                    {
                        mapper.WriteInt(wr, row.Limit.Start);
                        mapper.WriteInt(wr, row.Limit.MaxCount);
                    }

                    mapper.WriteInt(wr, row.Filter != null ? 1 : 0);
                    if (row.Filter != null)
                    {
                        var field = this.fields[row.Filter.Field];
                        mapper.WriteInt(wr, row.Filter.Field);
                        if (field.Type == FieldType.Int)
                        {
                            mapper.WriteInt(wr, row.Filter.Value);
                        }
                        else if (field.Type == FieldType.String)
                        {
                            mapper.WriteString(wr, row.Filter.Value);
                        }
                    }

                    mapper.WriteInt(wr, row.Sort != null ? 1 : 0);
                    if (row.Sort != null)
                    {
                        mapper.WriteInt(wr, row.Sort.Field);
                        mapper.WriteInt(wr, (int)row.Sort.Order);
                    }
                }
            }

            return array;
        }

        public IRowMatcher ReadRowMatcher(BinaryReader reader)
        {
            IRowMatcher matcher = new RowMatcher();

            var mapper = new TypeMapperDefault();
            if ((int)mapper.ReadInt(reader) == 1)
            {
                matcher.LimitBy(
                    (int)mapper.ReadInt(reader),
                    (int)mapper.ReadInt(reader));
            }

            if ((int)mapper.ReadInt(reader) == 1)
            {
                int index = (int)mapper.ReadInt(reader);
                var field = this.fields[index];
                object val = null;

                if (field.Type == FieldType.Int)
                {
                    val = mapper.ReadInt(reader);
                }
                else if (field.Type == FieldType.String)
                {
                    val = mapper.ReadString(reader);
                }

                matcher.FilterBy(
                    index, 
                    val);
            }

            if ((int)mapper.ReadInt(reader) == 1)
            {
                matcher.SortBy(
                    (int)mapper.ReadInt(reader),
                    (SortOrder)mapper.ReadInt(reader));
            }

            return matcher;
        }
    }
}
