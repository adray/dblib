﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace DBLib
{
    public interface IDatabase : IDisposable
    {
        event EventHandler<RowChangeEventArgs<IPersistentRow>> RowAdded;
        event EventHandler<RowChangeEventArgs<IPersistentRow>> RowDeleted;
        event EventHandler<RowChangeEventArgs<IPersistentRow>> RowUpdated;
        bool AtomicAddRow(IRow row);
        IRow AtomicGetById(int id);
        void AtomicDeleteRow(int id);
        void AtomicUpdateRow(int id, IRow row);
        IList<IRow> AtomicLookup(IRowMatcher matcher);
        void AddIndex(Index index, IndexSpec spec);
    }

    public enum RowUpdate
    {
        Add,
        Update,
        Delete,
    }

    public class RowChangeEventArgs<T> : EventArgs  where T : IRow 
    {
        public RowUpdate Update { get; private set; }
        public T Row { get; private set; }

        public RowChangeEventArgs(RowUpdate update, T row)
        {
            Update = update;
            Row = row;
        }
    }

    internal class AsyncQueue : IDisposable
    {
        private BlockingCollection<Action> queue = new BlockingCollection<Action>();
        private Thread worker;

        public AsyncQueue()
        {
            worker = new Thread(() =>
            {
                while (true)
                {
                    queue.Take()();
                }
            });
            worker.Start();
        }

        public void Queue(Action task)
        {
            queue.Add(task);
        }

        public void Dispose()
        {
            if (this.worker != null)
            {
                this.worker.Abort();
                this.worker = null;
            }
        }
    }

    internal class Database : IDatabase, IDisposable
    {
        private class Scheme
        {
            public Type Type { get; set; }
            public string Name { get; set; }
        }

        private class IndexPair
        {
            public Index Index { get; set; }
            public IndexSpec Spec { get; set; }
        }

        private AsyncQueue queue = new AsyncQueue();
        private List<Scheme> scheme = new List<Scheme>();
        private int rowCount = -1;
        private IndexById byid;
        private List<IndexPair> indices = new List<IndexPair>();

        private event EventHandler<RowChangeEventArgs<IPersistentRow>> rowAdded;
        private event EventHandler<RowChangeEventArgs<IPersistentRow>> rowDeleted;
        private event EventHandler<RowChangeEventArgs<IPersistentRow>> rowUpdated;

        // The events could be added/removed during an atomic operation, therefore these must be queued up.

        public event EventHandler<RowChangeEventArgs<IPersistentRow>> RowAdded
        {
            add
            {
                this.queue.Queue(() => rowAdded += value);
            }
            remove
            {
                this.queue.Queue(() => rowAdded -= value);
            }
        }

        public event EventHandler<RowChangeEventArgs<IPersistentRow>> RowDeleted
        {
            add
            {
                this.queue.Queue(() => rowDeleted += value);
            }
            remove
            {
                this.queue.Queue(() => rowDeleted -= value);
            }
        }

        public event EventHandler<RowChangeEventArgs<IPersistentRow>> RowUpdated
        {
            add
            {
                this.queue.Queue(() => rowUpdated += value);
            }
            remove
            {
                this.queue.Queue(() => rowUpdated -= value);
            }
        }

        private class Row : IPersistentRow
        {
            private readonly int id;
            private List<object> Values { get; set; }

            public Row(int id, int fieldCount)
            {
                this.id = id;
                this.Values = new List<object>(fieldCount);
                for (int i = 0; i < fieldCount; i++)
                    this.Values.Add(null);
            }

            public int RowID
            {
                get { return this.id; }
            }

            public object GetField(int id)
            {
                return Values[id];
            }

            public void SetField(int id, object value)
            {
                Values[id] = value;
            }
        }

        public Database()
        {
            this.byid = new IndexById(this);
        }

        public void Dispose()
        {
            if (queue != null)
            {
                queue.Dispose();
                queue = null;
            }
        }

        public void AddInt(string name)
        {
            this.scheme.Add(new Scheme()
                {
                    Type = typeof(Int32),
                    Name = name,
                });
        }

        public void AddString(string name)
        {
            this.scheme.Add(new Scheme()
            {
                Type = typeof(String),
                Name = name,
            });
        }

        private void OnRowAdded(IPersistentRow row)
        {
            if (this.rowAdded != null)
            {
                this.rowAdded(this, new RowChangeEventArgs<IPersistentRow>(RowUpdate.Add, row));
            }
        }

        private void OnRowDeleted(int index)
        {
            if (this.rowDeleted != null)
            {
                IRowMatcher matcher = new RowMatcher().LimitBy(index, 1);

                var result = this.byid.Lookup(matcher).FirstOrDefault();

                if (result != null)
                {
                    this.rowDeleted(this, new RowChangeEventArgs<IPersistentRow>(RowUpdate.Delete, result));
                }
            }
        }

        private void OnRowUpdated(int index, IRow row)
        {
            if (this.rowUpdated != null)
            {
                IRowMatcher matcher = new RowMatcher().LimitBy(index, 1);

                var result = this.byid.Lookup(matcher).FirstOrDefault();
                if (result != null)
                {
                    int count = 0;
                    foreach (var field in scheme)
                    {
                        result.SetField(count, row.GetField(count));
                        count++;
                    }

                    this.rowUpdated(this, new RowChangeEventArgs<IPersistentRow>(RowUpdate.Update, result));
                }
            }
        }

        public bool AtomicAddRow(IRow row)
        {
            int id = Interlocked.Increment(ref rowCount);
            Row newRow = new Row(id, scheme.Count);
            
            int count = 0;
            foreach (var field in scheme)
            {
                newRow.SetField(count, row.GetField(count));
                count++;
            }

            this.queue.Queue(() => OnRowAdded(newRow));
            return true;
        }

        public void AtomicDeleteRow(int index)
        {
            this.queue.Queue(() => OnRowDeleted(index));
        }

        public IRow AtomicGetById(int id)
        {
            IRowMatcher matcher = new RowMatcher().LimitBy(id, 1);
            
            IRow result = this.byid.Lookup(matcher).FirstOrDefault();

            return result;
        }

        public void AtomicUpdateRow(int id, IRow row)
        {
            this.queue.Queue(() => OnRowUpdated(id, row));
        }

        public IList<IRow> AtomicLookup(IRowMatcher matcher)
        {
            List<IRow> result = new List<IRow>();

            foreach (var index in indices)
            {
                if (index.Spec.CanIndex(matcher))
                {
                    result.AddRange(index.Index.Lookup(matcher));
                }
            }
            
            return result;
        }

        public void AddIndex(Index index, IndexSpec spec)
        {
            this.indices.Add(new IndexPair()
                {
                    Index = index,
                    Spec = spec,
                });
        }
    }
}
