﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace DBLib
{
    public interface IRowMatcher
    {
        SortBy Sort { get; set; }
        FilterBy Filter { get; set; }
        LimitTo Limit { get; set; }

        IRowMatcher SortBy(int field, SortOrder order);
        IRowMatcher FilterBy(int field, object value);
        IRowMatcher LimitBy(int start, int maxcount);
    }

    public class SortBy
    {
        public SortOrder Order { get; set; }
        public int Field { get; set; }
    }

    public class FilterBy
    {
        public object Value { get; set; }
        public int Field { get; set; }
    }

    public class LimitTo
    {
        public int MaxCount { get; set; }
        public int Start { get; set; }
    }

    public enum SortOrder
    {
        Ascending,
        Decending,
    }

    public class RowMatcher : IRowMatcher
    {
        public SortBy Sort { get; set; }
        public FilterBy Filter { get; set; }
        public LimitTo Limit { get; set; }

        public IRowMatcher SortBy(int field, SortOrder order)
        {
            this.Sort = new SortBy()
            {
                Order = order,
                Field = field,
            };
            return this;
        }

        public IRowMatcher FilterBy(int field, object value)
        {
            this.Filter = new FilterBy()
            {
                Value = value,
                Field = field,
            };
            return this;
        }

        public IRowMatcher LimitBy(int start, int maxcount)
        {
            this.Limit = new LimitTo()
            {
                Start = start,
                MaxCount = maxcount,
            };
            return this;
        }
    }

    public class IndexSpec
    {
        public SortBy Sort { get; set; }
        public int? Filter { get; set; }

        public bool CanIndex(IRowMatcher matcher)
        {
            bool ret = true;
            ret &= (Filter == null && matcher.Filter == null) ||
                (Filter != null && matcher.Filter != null && Filter == matcher.Filter.Field);
            ret &= (Sort == null && matcher.Sort == null) ||
                (Sort != null && matcher.Sort != null && Sort.Field == matcher.Sort.Field &&
                Sort.Order == matcher.Sort.Order);
            return ret;
        }
    }

    public abstract class Index
    {
        private IDatabase database;
        private ReaderWriterLockSlim @lock = new ReaderWriterLockSlim();        

        public void Hook(IDatabase database)
        {
            this.database = database;
            this.database.RowAdded += database_RowAdded;
            this.database.RowDeleted += database_RowDeleted;
            this.database.RowUpdated += database_RowUpdated;
        }

        void database_RowUpdated(object sender, RowChangeEventArgs<IPersistentRow> e)
        {
            @lock.EnterWriteLock();
            OnRowUpdated(e.Row);
            @lock.ExitWriteLock();
        }

        void database_RowDeleted(object sender, RowChangeEventArgs<IPersistentRow> e)
        {
            @lock.EnterWriteLock();
            OnRowDeleted(e.Row);
            @lock.ExitWriteLock();
        }

        void database_RowAdded(object sender, RowChangeEventArgs<IPersistentRow> e)
        {
            @lock.EnterWriteLock();
            OnRowAdded(e.Row);
            @lock.ExitWriteLock();
        }

        public IDatabase Database
        {
            get
            {
                return database;
            }
        }

        public IList<IPersistentRow> Lookup(IRowMatcher matcher)
        {
            @lock.EnterReadLock();
            var list = OnLookup(matcher);
            @lock.ExitReadLock();
            return list;
        }

        protected abstract IList<IPersistentRow> OnLookup(IRowMatcher matcher);

        public abstract void OnRowAdded(IPersistentRow row);

        public abstract void OnRowDeleted(IPersistentRow row);

        public abstract void OnRowUpdated(IPersistentRow row);
    }

    public class IndexById : Index
    {
        private Dictionary<int, IPersistentRow> rows = new Dictionary<int, IPersistentRow>();

        public IndexById(IDatabase database)
        {
            this.Hook(database);
        }

        protected override IList<IPersistentRow> OnLookup(IRowMatcher matcher)
        {
            List<IPersistentRow> result = new List<IPersistentRow>();
            for (int i = Math.Max(0, matcher.Limit.Start); i < Math.Min(rows.Count, matcher.Limit.MaxCount + matcher.Limit.Start); i++)
            {
                IPersistentRow row = null;
                if (rows.TryGetValue(i, out row))
                {
                    result.Add(row);
                }
            }

            return result;
        }

        public override void OnRowAdded(IPersistentRow row)
        {
            this.rows.Add(row.RowID, row);
        }

        public override void OnRowDeleted(IPersistentRow row)
        {
            this.rows.Remove(row.RowID);
        }

        public override void OnRowUpdated(IPersistentRow row)
        {
            // Do nothing
        }
    }

    public class NonUniqueHashIndex : Index
    {
        private int key;
        private Dictionary<int, object> keysByIndex = new Dictionary<int, object>();
        private Hashtable rows = new Hashtable();
        private Func<Index> indexCallback;

        public int Key
        {
            get
            {
                return this.key;
            }
        }

        public NonUniqueHashIndex(int key, Func<Index> indexCallback)
        {
            this.key = key;
            this.indexCallback = indexCallback;
        }

        public NonUniqueHashIndex(IDatabase database, int key, Func<Index> indexCallback) : this(key, indexCallback)
        {
            this.Hook(database);
        }

        protected override IList<IPersistentRow> OnLookup(IRowMatcher matcher)
        {
            List<IPersistentRow> results = new List<IPersistentRow>();
            
            var val = matcher.Filter.Value;
            Index index = null;
            if (rows.ContainsKey(val))
            {
                index = rows[val] as Index;
                results.AddRange(index.Lookup(matcher));
            }

            return results;
        }

        public override void OnRowAdded(IPersistentRow row)
        {
            object val = GetValue(row);
            Index index = null;
            if (rows.ContainsKey(val))
            {
                index = rows[val] as Index;
            }
            else
            {
                index = indexCallback();
                this.rows.Add(val, index);
            }
            index.OnRowAdded(row);
            keysByIndex.Add(row.RowID, val);
        }

        public override void OnRowDeleted(IPersistentRow row)
        {
            object val = GetValue(row);
            Index index = null;
            if (rows.ContainsKey(val))
            {
                index = rows[val] as Index;
                index.OnRowDeleted(row);
            }
            keysByIndex.Remove(row.RowID);
        }

        public override void OnRowUpdated(IPersistentRow row)
        {
            object oldKey = keysByIndex[row.RowID];
            object val = GetValue(row);

            Index index = rows[oldKey] as Index;
            if (val.Equals(oldKey))
            {
                index.OnRowUpdated(row);
            }
            else
            {
                // Update the index to use latest key.
                index.OnRowUpdated(row);
                // Delete the index now the key is updated.
                index.OnRowDeleted(row);
                // Add to new index.
                index = rows[val] as Index;
                index.OnRowAdded(row);
            }
        }

        private object GetValue(IPersistentRow row)
        {
            return row.GetField(key);
        }
    }

    public class UniqueHashIndex : Index
    {
        private int key;
        private Dictionary<int, object> keysByIndex = new Dictionary<int, object>();
        private Hashtable rows = new Hashtable();

        public int Key
        {
            get
            {
                return this.key;
            }
        }

        public UniqueHashIndex(int key)
        {
            this.key = key;
        }

        public UniqueHashIndex(IDatabase database, int key) : this(key)
        {
            this.Hook(database);
        }

        protected override IList<IPersistentRow> OnLookup(IRowMatcher matcher)
        {
            List<IPersistentRow> results = new List<IPersistentRow>();

            var val = matcher.Filter.Value;

            if (rows.ContainsKey(val))
            {
                IPersistentRow row = rows[val] as IPersistentRow;
                results.Add(row);
            }

            return results;
        }

        public override void OnRowAdded(IPersistentRow row)
        {
            var val = GetValue(row);
            this.rows.Add(val, row);
            this.keysByIndex.Add(row.RowID, val);
        }

        public override void OnRowDeleted(IPersistentRow row)
        {
            var val = GetValue(row);
            this.rows.Remove(val);
            this.keysByIndex.Remove(row.RowID);
        }

        public override void OnRowUpdated(IPersistentRow row)
        {
            var oldkey = this.keysByIndex[row.RowID];
            var val = GetValue(row);
            if (!oldkey.Equals(val))
            {
                this.keysByIndex[row.RowID] = val;
            }
            this.rows[val] = row;
        }

        private object GetValue(IPersistentRow row)
        {
            return row.GetField(key);
        }
    }

    public class SortedIndex<T> : Index where T : IComparable
    {
        private class Key 
        {
            public T Primary;
            public int Secondary;

            public override int GetHashCode()
            {
                return Primary.GetHashCode() ^ Secondary;
            }
        }

        private class KeyComparerAcending : IComparer<Key>
        {
            public int Compare(Key x, Key y)
            {
                if (x.Primary.Equals(y.Primary))
                {
                    return x.Secondary.CompareTo(y.Secondary);
                }
                return x.Primary.CompareTo(y.Primary);
            }
        }

        private class KeyComparerDecending : IComparer<Key>
        {
            public int Compare(Key x, Key y)
            {
                if (x.Primary.Equals(y.Primary))
                {
                    return -x.Secondary.CompareTo(y.Secondary);
                }
                return -x.Primary.CompareTo(y.Primary);
            }
        }

        private static KeyComparerDecending decending = new KeyComparerDecending();
        private static KeyComparerAcending acending = new KeyComparerAcending();
        private Dictionary<IPersistentRow, Key> keys = new Dictionary<IPersistentRow, Key>();
        private SortedList<Key, IPersistentRow> rows;
        private int key;

        public SortedIndex(int key, SortOrder order = SortOrder.Decending)
        {
            this.key = key;
            if (order == SortOrder.Ascending)
            {
                this.rows = new SortedList<Key, IPersistentRow>(acending);
            }
            else if (order == SortOrder.Decending)
            {
                this.rows = new SortedList<Key, IPersistentRow>(decending);
            }
        }

        public SortedIndex(IDatabase database, int key, SortOrder order = SortOrder.Decending) : this(key, order)
        {
            this.Hook(database);
        }

        protected override IList<IPersistentRow> OnLookup(IRowMatcher matcher)
        {
            List<IPersistentRow> result = new List<IPersistentRow>();
            for (int i = Math.Max(0, matcher.Limit.Start); i < Math.Min(rows.Count, matcher.Limit.Start + matcher.Limit.MaxCount); i++)
            {
                result.Add(rows.Values[i]);
            }
            return result;
        }

        public override void OnRowAdded(IPersistentRow row)
        {
            Key key = new Key();
            key.Primary = (T)row.GetField(this.key);
            key.Secondary = row.RowID;
            this.rows.Add(key, row);
            this.keys.Add(row, key);
        }

        public override void OnRowDeleted(IPersistentRow row)
        {
            Key key = new Key();
            key.Primary = (T)row.GetField(this.key);
            key.Secondary = row.RowID;
            this.rows.Remove(key);
            this.keys.Remove(row);
        }

        public override void OnRowUpdated(IPersistentRow row)
        {
            Key oldKey = this.keys[row];
            if (this.rows.Remove(oldKey))
            {
                Key key = new Key();
                key.Primary = (T)row.GetField(this.key);
                key.Secondary = row.RowID;
                this.rows.Add(key, row);

                this.keys[row] = key;
            }
            else
            {
                throw new Exception("Key could not be row: " + row.RowID);
            }
        }
    }
}
