﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DBLib
{
    public interface IDBConnection
    {
        void AtomicAdd(IRow row);
        void AtomicDeleteById(int id);
        void AtomicUpdateById(int id, IRow row);
        DataRow AtomicGetById(int id);
        IRowSchema DownloadSchema(string schema);
        IRowSchema Schema { get; set; }
        event EventHandler<RowChangeEventArgs<DataRow>> RowAdded;
        event EventHandler ConnectionRecovered;
        IList<DataRow> AtomicLookup(IRowMatcher matcher);
        void CloseConnection();
    }

    public interface IDBEndPoint
    {
        void ProcessAsync(IDatabase database, IRowSchema schema);
        bool ConnectionActive { get; }
    }

    internal class ConnectionFactory
    {
        public static IDBEndPoint CreateEndPoint(NetConnection connection)
        {
            return new DBEndPoint(connection);
        }

        public static IDBConnection CreateDBConnection(string host, int port)
        {
            return new DBConnection(host, port);
        }

        private enum Operation
        {
            Add,
            Update,
            Get,
            Lookup,
            Delete,
            DownloadSchema,
            Subscribe,
        }

        private enum Result
        {
            Pass,
            Fail,
        }

        private class DBEndPoint : IDBEndPoint
        {
            private Publisher publisher;
            private NetConnection conn;

            public DBEndPoint(NetConnection conn)
            {
                this.conn = conn;
            }

            public bool ConnectionActive
            {
                get
                {
                    return this.conn.ConnectionActive;
                }
            }

            private void ProcessMessage(IDatabase database, IRowSchema schema, NetMessage msg)
            {
                using (MemoryStream memory = new MemoryStream(msg.Payload))
                {
                    Operation op = (Operation)memory.ReadByte();

                    NetMessage reply = null;

                    using (BinaryReader reader = new BinaryReader(memory))
                    {
                        switch (op)
                        {
                            case Operation.DownloadSchema:
                                {
                                    int schemaBytes = reader.ReadByte();
                                    string name = Encoding.Default.GetString(reader.ReadBytes(schemaBytes));
                                    IRowSchema scheme = DBFactory.LocateSchema(name);

                                    if (scheme != null)
                                    {
                                        reply = new NetMessage();
                                        reply.Payload = scheme.ToBytes();

                                        this.conn.SendAsync(reply);
                                    }
                                }
                                break;
                            case Operation.Add:
                                {
                                    var rowReader = new RowReader(reader);
                                    database.AtomicAddRow(schema.CreateRow(rowReader));
                                }
                                break;
                            case Operation.Delete:
                                {
                                    int id = reader.ReadInt32();
                                    database.AtomicDeleteRow(id);
                                }
                                break;
                            case Operation.Update:
                                {
                                    int id = reader.ReadInt32();
                                    var rowReader = new RowReader(reader);
                                    database.AtomicUpdateRow(id, schema.CreateRow(rowReader));
                                }
                                break;
                            case Operation.Lookup:
                                {
                                    IRowMatcher matcher = schema.ReadRowMatcher(reader);
                                    var results = database.AtomicLookup(matcher);

                                    RowWriter rowWriter = new RowWriter();
                                    if (results.Count > 0)
                                    {
                                        rowWriter.WriteInt(Result.Pass);
                                        rowWriter.WriteInt(results.Count);
                                        foreach (var row in results)
                                        {
                                            schema.WriteRow(rowWriter, row);
                                        }
                                    }
                                    else
                                    {
                                        rowWriter.WriteInt(Result.Fail);
                                    }

                                    reply = new NetMessage();
                                    reply.Payload = rowWriter.Data;

                                    this.conn.SendAsync(reply);
                                }
                                break;
                            case Operation.Get:
                                {
                                    int id = reader.ReadInt32();
                                    IRow row = database.AtomicGetById(id);
                                    RowWriter rowWriter = new RowWriter();
                                    if (row != null)
                                    {
                                        rowWriter.WriteInt(Result.Pass);
                                        schema.WriteRow(rowWriter, row);
                                    }
                                    else
                                    {
                                        rowWriter.WriteInt(Result.Fail);
                                    }

                                    reply = new NetMessage();
                                    reply.Payload = rowWriter.Data;

                                    this.conn.SendAsync(reply);
                                }
                                break;
                            case Operation.Subscribe:

                                if (this.publisher == null)
                                {
                                    this.publisher = new Publisher(database, schema, this.conn);
                                }

                                break;
                        }
                    }
                }
            }

            private bool hooked = false;
            private Action<NetMessage> callback;

            public void ProcessAsync(IDatabase database, IRowSchema schema)
            {
                if (!hooked)
                {
                    this.conn.MessageRecieved += (o, e) =>
                    {
                        this.callback(e.Message);
                    };
                    this.hooked = true;
                }

                this.callback = (msg) =>
                {
                    if (msg != null) this.ProcessMessage(database, schema, msg);
                };
            }
        }

        private class DBConnection : IDBConnection
        {
            private AutoRetryConnection conn;
            private IRowSchema schema;
            public event EventHandler ConnectionRecovered;

            public IRowSchema Schema
            {
                get
                {
                    return this.schema;
                }
                set
                {
                    this.schema = value;
                }
            }

            public DBConnection(string host, int port)
            {
                this.conn = new AutoRetryConnection(host, port);
                this.conn.ConnectionDown += conn_ConnectionDown;
                this.conn.ConnectionUp += conn_ConnectionUp;
            }

            void conn_ConnectionUp(object sender, EventArgs e)
            {
                if (this.ConnectionRecovered != null)
                {
                    this.ConnectionRecovered(this, EventArgs.Empty);
                }

                if (subscribing)
                {
                    this.Subscribe();
                }
            }

            void conn_ConnectionDown(object sender, EventArgs e)
            {
                if (subscribing)
                {
                    this.conn.Connection.MessageRecieved -= Connection_MessageRecieved;
                }
            }

            public IRowSchema DownloadSchema(string schema)
            {
                NetMessage msg = new NetMessage();

                byte[] schemaBytes = Encoding.Default.GetBytes(schema);
                int length = schemaBytes.Length + sizeof(byte) * 2;
                msg.Payload = new byte[length];

                using (MemoryStream memory = new MemoryStream(msg.Payload))
                {
                    using (BinaryWriter writer = new BinaryWriter(memory))
                    {
                        writer.Write((byte)Operation.DownloadSchema);
                        writer.Write((byte)schema.Length);
                        writer.Write(schemaBytes);
                    }
                }
                
                NetMessage reply = this.conn.Connection.SendRecieve(msg);
                if (reply != null)
                {
                    return DBFactory.CreateSchema(schema, reply.Payload);
                }
                
                return null;
            }

            private NetMessage Prepare(IRow row, Operation op)
            {
                NetMessage msg = new NetMessage();

                RowWriter rowWriter = new RowWriter();
                Schema.WriteRow(rowWriter, row);
                byte[] payload = rowWriter.Data;
                msg.Payload = new byte[payload.Length + 1];

                using (MemoryStream memory = new MemoryStream(msg.Payload))
                {
                    using (BinaryWriter writer = new BinaryWriter(memory))
                    {
                        writer.Write((byte)op);
                        writer.Write(payload);
                    }
                }

                return msg;
            }

            public void AtomicAdd(IRow row)
            {
                this.conn.Connection.SendAsync(Prepare(row, Operation.Add));
            }

            public void AtomicUpdateById(int id, IRow row)
            {
                NetMessage msg = new NetMessage();

                RowWriter rowWriter = new RowWriter();
                Schema.WriteRow(rowWriter, row);
                byte[] payload = rowWriter.Data;
                msg.Payload = new byte[payload.Length + 1 + sizeof(int)];

                using (MemoryStream memory = new MemoryStream(msg.Payload))
                {
                    using (BinaryWriter writer = new BinaryWriter(memory))
                    {
                        writer.Write((byte)Operation.Update);
                        writer.Write(id);
                        writer.Write(payload);
                    }
                }

                this.conn.Connection.SendAsync(msg);
            }

            public DataRow AtomicGetById(int row)
            {
                NetMessage msg = new NetMessage();
                msg.Payload = new byte[sizeof(int) + 1];

                using (MemoryStream memory = new MemoryStream(msg.Payload))
                {
                    using (BinaryWriter writer = new BinaryWriter(memory))
                    {
                        writer.Write((byte)Operation.Get);
                        writer.Write(row);
                    }
                }
                
                NetMessage reply = this.conn.Connection.SendRecieve(msg);
                if (reply != null)
                {
                    using (MemoryStream memory = new MemoryStream(reply.Payload))
                    {
                        using (BinaryReader reader = new BinaryReader(memory))
                        {
                            if ((int)reader.ReadInt32() == (int)Result.Pass)
                            {
                                RowReader rowReader = new RowReader(reader);
                                return this.Schema.CreateRow(rowReader);
                            }
                        }
                    }
                }
                return null;
            }

            private void Subscribe()
            {
                this.subscribing = true;
                NetMessage msg = new NetMessage();

                msg.Payload = new byte[1];

                using (MemoryStream memory = new MemoryStream(msg.Payload))
                {
                    using (BinaryWriter writer = new BinaryWriter(memory))
                    {
                        writer.Write((byte)Operation.Subscribe);
                    }
                }

                this.conn.Connection.MessageRecieved += Connection_MessageRecieved;

                this.conn.Connection.SendAsync(msg);
            }

            private bool subscribing;
            public event EventHandler<RowChangeEventArgs<DataRow>> rowAdded;
            public event EventHandler<RowChangeEventArgs<DataRow>> rowUpdated;

            void Connection_MessageRecieved(object sender, MessageRecievedEventArgs e)
            {
                var msg = e.Message;
                if (msg != null)
                {
                    using (MemoryStream memory = new MemoryStream(msg.Payload))
                    {
                        using (BinaryReader reader = new BinaryReader(memory))
                        {
                            RowUpdate update = (RowUpdate)reader.ReadInt32();
                            RowReader rowReader = new RowReader(reader);
                            if (update == RowUpdate.Add)
                            {
                                this.OnRowAdded(schema.CreateRow(rowReader));
                            }
                            else if (update == RowUpdate.Update)
                            {
                                this.OnRowUpdate(schema.CreateRow(rowReader));
                            }
                        }
                    }
                }
            }

            private void OnRowAdded(DataRow row)
            {
                if (rowAdded != null)
                {
                    rowAdded(this, new RowChangeEventArgs<DataRow>(RowUpdate.Add, row));
                }
            }

            private void OnRowUpdate(DataRow row)
            {
                if (rowUpdated != null)
                {
                    rowUpdated(this, new RowChangeEventArgs<DataRow>(RowUpdate.Update, row));
                }
            }

            public event EventHandler<RowChangeEventArgs<DataRow>> RowAdded
            {
                add
                {
                    if (!this.subscribing)
                    {
                        this.Subscribe();
                    }
                    rowAdded += value;
                }
                remove
                {
                    rowAdded -= value;
                }
            }

            public void AtomicDeleteById(int id)
            {
                NetMessage msg = new NetMessage();

                msg.Payload = new byte[1 + sizeof(int)];

                using (MemoryStream memory = new MemoryStream(msg.Payload))
                {
                    using (BinaryWriter writer = new BinaryWriter(memory))
                    {
                        writer.Write((byte)Operation.Delete);
                        writer.Write(id);
                    }
                }

                this.conn.Connection.SendAsync(msg);
            }

            public IList<DataRow> AtomicLookup(IRowMatcher matcher)
            {
                byte[] bytes = schema.WriteRowMatcher(matcher);

                NetMessage msg = new NetMessage();
                msg.Payload = new byte[bytes.Length + sizeof(byte)];

                using (MemoryStream memory = new MemoryStream(msg.Payload))
                {
                    using (BinaryWriter writer = new BinaryWriter(memory))
                    {
                        writer.Write((byte)Operation.Lookup);
                        writer.Write(bytes);
                    }
                }

                List<DataRow> rows = new List<DataRow>();
                
                NetMessage reply = this.conn.Connection.SendRecieve(msg);
                if (reply != null)
                {
                    using (MemoryStream memory = new MemoryStream(reply.Payload))
                    {
                        using (BinaryReader reader = new BinaryReader(memory))
                        {
                            if ((int)reader.ReadInt32() == (int)Result.Pass)
                            {
                                int results = reader.ReadInt32();
                                for (int i = 0; i < results; i++)
                                {
                                    RowReader rowReader = new RowReader(reader);
                                    rows.Add(this.Schema.CreateRow(rowReader));
                                }
                            }
                        }
                    }
                }
                
                return rows;
            }

            public void CloseConnection()
            {
                this.conn.Connection.Close();
            }
        }
    }
}
