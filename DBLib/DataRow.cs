﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DBLib
{
    public interface IRow
    {
        object GetField(int id);
        void SetField(int id, object value);
    }

    public interface IPersistentRow : IRow
    {
        int RowID { get; }
    }

    public class DataRow : IRow
    {
        private class Field
        {
            public Object Value { get; set; }
            public Type Type { get; set; }
        }

        private Dictionary<string, Field> fields = new Dictionary<string, Field>();
        private List<Field> indexFields = new List<Field>();

        internal DataRow()
        {
        }

        private void AddField(string name, Type type)
        {
            Field field = new Field();
            field.Type = type;
            fields.Add(name, field);
            indexFields.Add(field);
        }

        internal void AddInt(string name)
        {
            AddField(name, typeof(Int32));
        }

        internal void AddString(string name)
        {
            AddField(name, typeof(String));
        }

        public object GetField(int id)
        {
            return indexFields[id].Value;
        }

        public T GetField<T>(string name)
        {
            return (T)fields[name].Value;
        }

        public void SetField(int id, object value)
        {
            indexFields[id].Value = value;
        }

        public void SetField(string name, object value)
        {
            fields[name].Value = value;
        }
    }

    public interface ITypeMapper
    {
        Type Int { get; }
        Type String { get; }
        object ReadInt(BinaryReader reader);
        object ReadString(BinaryReader reader);
        void WriteInt(BinaryWriter writer, object value);
        void WriteString(BinaryWriter writer, object value);
        int MeasureString(object value);
    }

    public class TypeMapperDefault : ITypeMapper
    {
        public Type Int
        {
            get { return typeof(Int32); }
        }

        public Type String
        {
            get { return typeof(String); }
        }

        public object ReadInt(BinaryReader reader)
        {
            return reader.ReadInt32();
        }

        public object ReadString(BinaryReader reader)
        {
            int length = reader.ReadByte();
            return Encoding.Default.GetString(reader.ReadBytes(length));
        }

        public void WriteInt(BinaryWriter writer, object value)
        {
            if (value == null)
            {
                writer.Write((int)0);
            }
            else
            {
                writer.Write((int)value);
            }
        }

        public void WriteString(BinaryWriter writer, object value)
        {
            string val = value as string;
            if (string.IsNullOrEmpty(val))
            {
                writer.Write((byte)0);
            }
            else
            {
                byte[] strBytes = Encoding.Default.GetBytes(val);
                writer.Write((byte)strBytes.Length);
                writer.Write(strBytes);
            }
        }

        public int MeasureString(object value)
        {
            string val = value as string;
            if (string.IsNullOrEmpty(val))
            {
                return 1;
            }
            return Encoding.Default.GetByteCount(value as string) + 1;
        }
    }

    public class RowWriter
    {
        private int size;
        private byte[] data;
        private ITypeMapper mapper = new TypeMapperDefault();
        private Queue<Action<BinaryWriter>> queue = new Queue<Action<BinaryWriter>>();

        public RowWriter()
        {
        }

        public void WriteFieldCount(int fieldCount)
        {
            size++;
            queue.Enqueue((writer) => writer.Write((byte)fieldCount));
        }

        public void WriteInt(object value)
        {
            size += sizeof(int);
            queue.Enqueue((writer) => mapper.WriteInt(writer, value));
        }

        public void WriteString(object value)
        {
            size += mapper.MeasureString(value);
            queue.Enqueue((writer) => mapper.WriteString(writer, value));
        }

        public byte[] Data
        {
            get
            {
                if (this.data != null)
                    return data;

                this.data = new byte[size];

                using (MemoryStream memory = new MemoryStream(data))
                {
                    using (BinaryWriter writer = new BinaryWriter(memory))
                    {
                        Action<BinaryWriter> action = null;
                        while (this.queue.Count > 0)
                        {
                            action = this.queue.Dequeue();
                            action(writer);
                        }

                        return this.data;
                    }
                }
            }
        }
    }

    public class RowReader
    {
        private BinaryReader reader;
        private int fieldCount;
        private ITypeMapper mapper = new TypeMapperDefault();

        public RowReader(byte[] data)
        {
            MemoryStream memory = new MemoryStream(data);
            reader = new BinaryReader(memory);
            this.GetFieldCount();
        }

        public RowReader(BinaryReader reader)
        {
            this.reader = reader;
            this.GetFieldCount();
        }

        private void GetFieldCount()
        {
            this.fieldCount = reader.ReadByte();
        }

        public object ReadInt()
        {
            return mapper.ReadInt(reader);
        }

        public object ReadString()
        {
            return mapper.ReadString(reader);
        }

        public int Count
        {
            get
            {
                return this.fieldCount;
            }
        }
    }
}
