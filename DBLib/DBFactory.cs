﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBLib
{
    public static class DBFactory
    {
        private class DBContext
        {
            public Dictionary<string, IRowSchema> schema = new Dictionary<string, IRowSchema>();
        }

        private static DBContext Default = new DBContext();
        private static DBContext Current = Default;

        public static void SwitchContext()
        {
            Current = new DBContext();
        }

        public static IDBConnection CreateConnection(string host, int port)
        {
            return ConnectionFactory.CreateDBConnection(host, port);
        }

        public static IDBEndPoint CreateEndPoint(NetConnection conn)
        {
            return ConnectionFactory.CreateEndPoint(conn);
        }

        public static IRowSchema CreateSchema(string name)
        {
            var scheme = LocateSchema(name);
            if (scheme != null)
            {
                return null; // operation failed
            }

            scheme = new RowSchema(name);
            Current.schema.Add(name, scheme);
            return scheme;
        }

        public static IRowSchema LocateSchema(string name)
        {
            IRowSchema ret = null;
            Current.schema.TryGetValue(name, out ret);
            return ret;
        }

        public static IRowSchema CreateSchema(string name, byte[] bytes)
        {
            var scheme = LocateSchema(name);
            if (scheme != null)
            {
                return null; // operation failed
            }

            scheme = new RowSchema(name, bytes);
            Current.schema.Add(name, scheme);
            return scheme;
        }
    }
}
