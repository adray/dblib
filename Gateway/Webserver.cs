﻿using Core;
using DBLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gateway
{
    public class WebServer : SubscriberProcess
    {        
        private HttpListener listener = new HttpListener();
        private IRowSchema schema;

        public WebServer() : base("Gateway", 9000, "DBServer")
        {
        }

        protected override void OnStart()
        {
            base.OnStart();            
            listener.Prefixes.Add("http://localhost:9000/");
            listener.Start();

            this.RunServer();
        }

        protected override void OnDatabaseAvailable()
        {
            base.OnDatabaseAvailable();

            if (this.schema == null)
            {
                schema = DBConnection.DownloadSchema("mySchema");
                DBConnection.Schema = schema;
            }
        }

        private static int? GetAsInt(System.Collections.Specialized.NameValueCollection collection, string name)
        {
            var field = collection[name];
            int @int;
            if (!Int32.TryParse(field, out @int))
            {
                return null;
            }
            return @int;
        }

        private void RunServer()
        {
            while (true)
            {
                var context = listener.GetContext();
                try
                {
                    using (StreamWriter writer = new StreamWriter(context.Response.OutputStream))
                    {
                        var result = System.Web.HttpUtility.ParseQueryString(context.Request.Url.Query);

                        if (context.Request.Url.LocalPath == "/Results")
                        {
                            var count = GetAsInt(result, "count") ?? 100;
                            var start = GetAsInt(result, "start");
                            var sort = GetAsInt(result, "sort") ?? 1;
                            var sortField = GetAsInt(result, "sortField") ?? 2;
                            var diff = result["diff"];

                            IList<DataRow> lookup = null;
                            
                            Stopwatch timer = new Stopwatch();
                            timer.Start();

                            if (this.schema != null)
                            {
                                IRowMatcher matcher = new RowMatcher()
                                    .LimitBy(start.GetValueOrDefault(), Math.Min(100, count))
                                    .SortBy(sortField, (SortOrder)sort);

                                if (!string.IsNullOrEmpty(diff))
                                {
                                    matcher.FilterBy(this.schema.FindByName("Difficulty"), diff);
                                }


                                lookup = DBConnection.AtomicLookup(matcher);
                            }

                            writer.WriteLine("<html><body><h1>Highscores Lookup</h1>");


                            writer.WriteLine("<form method=get action=/Results>");
                            writer.WriteLine("Count:<input type=text name=count width=100 value={0}>", count);
                            writer.WriteLine("Start:<input type=text name=start width=100 value={0}>", start.GetValueOrDefault());
                            writer.WriteLine("Difficulty:<input type=text name=diff width=100 value={0}>", diff ?? string.Empty);
                            writer.WriteLine("<input type=submit value=Submit>");

                            if (lookup == null || lookup.Count == 0)
                            {
                                writer.WriteLine("<p>No Results</p>");
                            }
                            else
                            {
                                writer.WriteLine("<table class=\"pure-table\"><thead><tr><th>#</th><th>Name</th><th>Score</th><th>Difficulty</th></tr></thead><tbody>");

                                int i = 1 + start.GetValueOrDefault();
                                foreach (var row in lookup)
                                {
                                    writer.WriteLine("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>", i++, row.GetField<string>("Name"), row.GetField<int>("Score"), row.GetField<string>("Difficulty"));
                                }
                                writer.WriteLine("</tbody></table>");
                            }

                            //writer.WriteLine("Current Time: " + DateTime.Now.ToString());
                            //writer.WriteLine("url : {0}", context.Request.Url.AbsolutePath);

                            //writer.WriteLine("<form method=post action=/form>");
                            //writer.WriteLine("<input type=text name=foo value=foovalue>");
                            //writer.WriteLine("<input type=submit name=bar value=barvalue>");

                            writer.WriteLine("Page generated in {0}ms", timer.ElapsedMilliseconds);
                            writer.WriteLine("</body></form>");
                        }
                        else if (context.Request.Url.LocalPath == "/Status")
                        {
                            Stopwatch timer = new Stopwatch();
                            timer.Start();

                            var processes = this.GetAllProcesses();
                            
                            writer.WriteLine("<html><body><h1>Service Status</h1>");

                            if (processes == null || processes.Count == 0)
                            {
                                writer.Write("<p>Processes down.</p>");
                            }
                            else
                            {
                                foreach (var process in processes)
                                {
                                    writer.Write("<p>{0} {1}:{2} {3}</p>", process.Name, process.Host, process.Port, process.Status);
                                }
                            }
                            
                            writer.WriteLine("Page generated in {0}ms", timer.ElapsedMilliseconds);
                            writer.WriteLine("</body>");
                        }
                    }
                    context.Response.Close();
                }
                catch (Exception)
                { }
            }
        }


        //public override void handlePOSTRequest(HttpProcessor p, StreamReader inputData)
        //{
        //    Console.WriteLine("POST request: {0}", p.http_url);
        //    string data = inputData.ReadToEnd();

        //    p.outputStream.WriteLine("<html><body><h1>test server</h1>");
        //    p.outputStream.WriteLine("<a href=/test>return</a><p>");
        //    p.outputStream.WriteLine("postbody: <pre>{0}</pre>", data);
        //}
    }
}
